def pad_left($len; $chr):
    (tostring | length) as $l
    | "\($chr * ([$len - $l, 0] | max) // "")\(.)"
    ;

def fromUStoISODate:
    split("/") | [.[2], (.[0] | pad_left(2; "0")), (.[1] | pad_left(2; "0"))] | join("-");

# Go to Activites > Options > download CSV

map(.date |= fromUStoISODate) | reverse
