#!/usr/bin/env bash

click() {
clickjs() {
cat <<EXECJS
	((querySelector, filterFn) => {
		[...document.querySelectorAll(querySelector)].filter(filterFn).map(el => el.click())
		return 'clicked ' + querySelector
	})('$1', ${2:-"el=>true"})
EXECJS
}
waitFor "$1"
chrome-cli execute "`clickjs "$@"`"
}

waitFor() {
	# wait until result page loads
	while test 0 -eq "$(chrome-cli execute "document.querySelectorAll('$@').length")"
	do
		printf . >&2
		sleep 1
	done
	echo
}

setValue() {
setValueJS() {
cat <<EXECJS
((querySelector, value, eventType) => {

	const qSA = querySelector => [...document.querySelectorAll(querySelector)]
	qSA(querySelector).forEach(element => {

	  element.focus()
      const { set: valueSetter } = Object.getOwnPropertyDescriptor(element, 'value') || {}
      const prototype = Object.getPrototypeOf(element)
      const { set: prototypeValueSetter } = Object.getOwnPropertyDescriptor(prototype, 'value') || {}

      if (prototypeValueSetter && valueSetter !== prototypeValueSetter) {
        prototypeValueSetter.call(element, value)
      } else if (valueSetter) {
        valueSetter.call(element, value)
      } else {
        throw new Error('The given element does not have a value setter')
      }

	  element.dispatchEvent(new Event(eventType, { bubbles: true }))

	})

})('$1', '$2', '${3:-input}')
EXECJS
}
waitFor "$1"
chrome-cli execute "`setValueJS "$@"`"
}
