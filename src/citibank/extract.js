#!/usr/bin/env node

(function main(Account) {
  function getAmount(debit, credit) {
    if (debit && credit)
      throw new Error(`${debit} and ${credit}`)

    if (debit)
      return '-' + debit.split(' ')[1]

    if (credit)
      return credit.split(' ')[1]
  }

  function toISODate(date) {
    // Citibank SG outputs date in dd/mm/yyyy
    // however new Date() takes input in mm/dd/yyyy
    return date.split('/').reverse().join('-')
  }

  const data = [...document.querySelectorAll('#postedTansactionTable table tbody tr')]
    .reverse()
    .map(el => [...el.children].map(ell => ell.innerText))
    .map(el => ({
      TransactionDate: toISODate(el[1]),
      Description: el[2],
      Amount: getAmount(el[3], el[4]),
      Account: `'${Account}'`,
      Balance: el[5].split(' ')[1],
    }))

  return JSON.stringify(data)
})('${Account}')
