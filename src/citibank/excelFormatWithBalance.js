var excelFormatWithBalance = $$('tr')
    .map(tr => tr.innerText.replace(/\n/g, '').split(/\t/))
    .map(tr => {
        return [
            tr[1].split('/').reverse().join('-'),
            tr[2],
            (tr[3] ? '-' + tr[3] : tr[4]).replace('SGD ', '').replace(',', ''),
            tr[5].replace('SGD ', '').replace(',', ''),
        ]
    })
    .reverse()
    .map(tr => tr.join('\t'))
    .join('\n')

window.copy(excelFormatWithBalance)
