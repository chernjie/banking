#!/usr/bin/env bash

DIRNAME=$(dirname "${BASH_SOURCE}")
source $DIRNAME/../chrome-cli/source.sh

citibank() {
    local timestamp=$(date '+%Y%m%d%H%I%S')

    click '#cmlink_lk_myCiti'
    click '#cmlink_AccountNameLink' 'el => el.innerText.match("'"$1"'".substr(-4))'

    waitFor '#cmlink_SeeMoreActivityLink,#noMoreTrans'
    chrome-cli execute "`cat $DIRNAME/prepare.js`"

    waitFor '#noMoreTrans'
    mkdir -p "$DIRNAME/../../citibank"
    chrome-cli execute "`cat $DIRNAME/extract.js | sed 's,\${Account},'"$1"',g'`" |
        json2csv --output "$DIRNAME/../../citibank/$1-$timestamp.csv"
}

for account in $(node $DIRNAME/accounts.js)
do
    citibank $account
done
