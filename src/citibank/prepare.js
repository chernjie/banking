#!/usr/bin/env node

async function seeMore(noMoreTransQuery, seeMoreLinkQuery) {
  function _seeMore(resolve, reject) {
    const noMoreTrans = document.querySelectorAll(noMoreTransQuery)
    if (noMoreTrans.length) return resolve(true)

    const seeMoreLink = document.querySelectorAll(seeMoreLinkQuery)
    if (seeMoreLink.length) seeMoreLink[0].click()

    if (counter++ > 10) return reject(`Timeout exceeded: ${counter}`)

    setTimeout(() => _seeMore(resolve, reject), 1000)
  }

  let counter = 0
  return new Promise(_seeMore)
}

seeMore('#noMoreTrans', '#cmlink_SeeMoreActivityLink')

null
