#!/usr/bin/env node

const csvtojson = require('csvtojson')

function transactionDetails(json) {
  return json
    .map(data => {
      const { Empty, ...others } = data
      return {
        ...others,
        TransactionDate: toISODate(others.TransactionDate),
        Balance: others.Balance || 0,
      }
    })
}

function toISODate(date) {
  // Citibank SG outputs date in dd/mm/yyyy
  // however new Date() takes input in mm/dd/yyyy
  return date.split('/').reverse().join('-')
}

module.exports = async function readFile(file) {
  const options = {}
  if (/ACCT|CHK/.test(file)) {
    options.headers = ['TransactionDate', 'Description', 'Amount', 'Empty', 'Account']
  }

  const json = await csvtojson(options).fromFile(file)
  return {
    file,
    transactions: transactionDetails(json),
  }
}
