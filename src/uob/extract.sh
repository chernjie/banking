#!/usr/bin/env bash

DIRNAME=$(dirname "${BASH_SOURCE}")
source $DIRNAME/../chrome-cli/source.sh

uob() {
  # go to Account Summary
  click 'ul.nav-main ul.nav.dk a[href*=processEnquiry]' 'el => el.innerText === "Account Summary"'

  # select account
  click '.account-summary-tables .text-md a' 'el => el.innerText.replace(/-/g, "").match("'"$1"'".substr(-4))'

  # select date range
  setValue '#frequency-account-summary' 'DR' 'change'
  click 'input#fromDate'
  # click '.datepicker-days th.prev[style*=visible]'
  # click '.datepicker-days th.prev[style*=visible]'
  # clickFirst '.datepicker-days td.day' 'el => !el.classList.contains("disabled") && !el.classList.contains("new")'
  setValue 'input#fromDate' "$2" 'input'
  # click '.datepicker-days th.next[style*=visible]'
  # click '.datepicker-days th.next[style*=visible]'
  # clickLast '.datepicker-days td.day' 'el => !el.classList.contains("disabled") && !el.classList.contains("new")'
  setValue 'input#toDate' "$3" 'input'

  # Search
  click '#filter-sticky button#btnsubmit.btn-primary'

  # Download
  sleep 2
  click '#filter-sticky button#btnsubmit.btn-default'
  sleep 2
}

fromDate=${fromDate:-'16/01/2020'}
toDate=${toDate:-'16/03/2020'}
echo hardcoded fromDate: $fromDate
echo hardcoded toDate: $toDate

for account in $(node $DIRNAME/accounts.js)
do
  uob $account "$fromDate" "$toDate"
done
