var XLSX = require('xlsx')

function accountInfo(worksheet) {

  const accountNumberRow = worksheet.filter(row => row.includes('Account Number:'))
  const accountTypeRow = worksheet.filter(row => row.includes('Account Type:'))

  return {
    accountNumber: accountNumberRow[0][1],
    currencyCode: accountNumberRow[0][2],
    accountType: accountTypeRow[0][1],
  }
}

function transactionDetails(worksheet, accountNumber) {

  return worksheet
    .filter(row => row.length > 4)
    .reduce((final, row) => {
      if (final.length) return {
        header: final.map(k => k.replace(/\s+/g, '')),
        data: [],
      }

      final.data.push(row.reduce((_row, value, index) => {
        _row[final.header[index]] = value
        return _row
      }, {}))
      return final
    })
    .data
    .map(el => ({ ...el, AccountNumber: accountNumber }))
    .reverse()
}

module.exports = function (file) {

  const workbook = XLSX.readFile(file)
  const worksheet = XLSX.utils.sheet_to_json(workbook.Sheets.Sheet0, { header: 1 })
  const _accountInfo = accountInfo(worksheet)

  return {
    ..._accountInfo,
    file,
    transactions: transactionDetails(worksheet, _accountInfo.accountNumber),
  }
}
