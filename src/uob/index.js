#!/usr/bin/env node

const readFile = require('./readFile')
const mergeFiles = require('../lib/mergeFiles')
const files = process.argv.splice(2)

Promise.all(files.map(readFile))
  .then(mergeFiles)
  .then(output => console.log(JSON.stringify(output)))
