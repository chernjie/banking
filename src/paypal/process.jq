def pad_left($len; $chr):
    (tostring | length) as $l
    | "\($chr * ([$len - $l, 0] | max) // "")\(.)"
    ;

def fromUStoISODate:
    split("/") | [.[2], (.[0] | pad_left(2; "0")), (.[1] | pad_left(2; "0"))] | join("-");

# Go to custom reports
# https://www.paypal.com/reports/dlog
# or https://business.paypal.com/merchantdata/consumerHome

map(.Date |= fromUStoISODate)
