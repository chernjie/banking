function hashify(el) {
  return Object.keys(el)
    .filter(key => key !== 'Balance')
    .filter(key => key !== 'Description')
    .map(key => el[key]).join('')
}

module.exports = function(files) {
  return files
    .map(file => file.transactions)
    .filter(txns => txns.length)
    .sort((txns1, txns2) => {
      if (!txns1[txns1.length - 1].TransactionDate) return 0
	  if (!txns2[txns2.length - 1].TransactionDate) return 0
      return new Date(txns1[txns1.length - 1].TransactionDate) - new Date(txns2[txns2.length - 1].TransactionDate)
    })
    .reduce((flatten, list) => {
      const hashed = flatten.reduce((hashed, el) => {
        return {
          ...hashed,
          [hashify(el)]: el,
        }
      }, {})

      return flatten.concat(list.filter(el => !hashed[hashify(el)]))
    }, [])
}
