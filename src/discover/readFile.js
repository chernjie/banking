#!/usr/bin/env node

const csvtojson = require('csvtojson')

function transactionDetails(json) {
  const txns = json
    .map(data => Object
      .keys(data)
      .reduce((final, key) => ({
        ...final,
        [key.replace(/\s+/g, '')]: data[key],
      }), {})
    )
    .map(data => ({
      ...data,
      TransactionDate: toISODate(data.TransactionDate),
    }))

  if (txns[txns.length - 1] && new Date(txns[0]) > new Date(txns[txns.length - 1])) {
    return txns.reverse()
  }
  return txns
}

function toISODate(date) {
  // new Date().toISOString() would have worked just fine
  // however while factoring in timezone
  // and resulting date might be one day early/late
  // depending on geographical location
  const parts = date.split('/')
  parts.unshift(parts.pop())
  return parts.join('-')
}

module.exports = async function readFile(file) {
  const json = await csvtojson().fromFile(file)
  return {
    file,
    transactions: transactionDetails(json),
  }
}
