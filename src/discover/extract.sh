#!/usr/bin/env bash

DIRNAME=$(dirname "${BASH_SOURCE}")
source $DIRNAME/../chrome-cli/source.sh

discover() {
    echo $1
    # $$('button.link[data-load="account-activity"]')[0].click()
}

for account in $(node $DIRNAME/accounts.js)
do
    discover $account
done
