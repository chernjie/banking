# banking

helps you manage and track your various financial accounts offline and transactions histories

## The Big Picture

1. Login to your bank account online (manual)
2. Download transaction histories from each accounts
3. See your consolidated networth in a beautiful chart

## Adapters

Each adapters refers to a particular online bank's CSV/XLS data format.

Adapters' steps includes:

- extract: download data from your online bank, data can be in any format you prefer that will be consumed by the next step
- post-extract/pre-deduplication processing
- de-duplication: more often than not, it is safer to download more history than you need but then you will have to deal with de-duplication
- grouping for charts

### Steps

```
extract.sh
	> file.csv
		> readFile.js
			> merged.js
				> sheets.js (for spreadsheet)
				> charts.js (for charts)
```

#### extract.sh

performs a browser download, or a browser scraping exercise

#### readFile.js

in-charge of reading the extracted files.

Challenges:
- sometimes banks do not include `accountNumber` in exports
- sometimes banks do not include balances in each row

#### merged.js

de-duplication by account numbers and transaction dates or file number

#### sheets.js

mutate `merged.js` and exported into CSV format

#### charts.js

preparation work for plotting a chart. Bucketing transactions by mutating them to conform to [#Transaction Object]

## Install

```
npm install https://bitbucket.org/chernjie/banking
```

## Contributions to adapters

Contributions are welcomed. Please specify a new directory under [./src](./src)

## Transaction Object

```json
{
  "Date": "YYYY-MM-DD",
  "Amount": "-100.00",
  "Currency": "SGD",
  "Balance": "1234.00",
  "Account": "Citibank"
}
```
